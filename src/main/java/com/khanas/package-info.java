/**
 * Provides the classes necessary to create an applet and
 * the classes an applet uses
 * to communicate with its applet context.
 * <p>
 * The applet framework involves two entities:
 * the applet and the applet context. An applet is an
 * embeddable window (see the
 * {@link com.khanas} class) with a few extra methods
 * that the applet context
 * can use to initialize, start, and stop the applet.
 *
 * @since 1.0
 * @see com.khanas
 */
package com.khanas;
