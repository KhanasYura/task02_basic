package com.khanas;

import java.util.Scanner;

/**
 * The first program for epam.
 * Task:
 * Write program (Maven project), which will pass requirements:
 * - User enter the interval (for example: [1;100]);
 * - Program prints odd numbers from start to the end of interval
 * and even from end to start;
 * Program prints the sum of odd and even numbers;
 * Program build Fibonacci numbers: F1 will be the biggest odd
 * number and F2 – the biggest even number,
 * user can enter the size of set (N);
 * Program prints percentage of odd and even Fibonacci numbers;
 */
final  class Application {

    /**
     * private Application - private constructor.
     */
    private Application() { }
    /**
     * The main method that does all the tasks.
     * @param args default args.
     */
    public static void main(final String[] args) {
        final int percentage = 100;
        Scanner sc = new Scanner(System.in);
        long sumOdd = 0;
        long sumEven = 0;
        System.out.println("Print start of the interval:");
        int start = sc.nextInt();
        System.out.println("Print end of the interval:");
        int end = sc.nextInt();
        for (int i = Math.min(start, end); i <= Math.max(start, end); i++) {
            if (i % 2 == 0) {
                System.out.print(i + " ");
                sumOdd += i;
            }
        }
        System.out.println();
        for (int i = Math.max(start, end); i >= Math.min(start, end); i--) {
            if (i % 2 != 0) {
                System.out.print(i + " ");
                sumEven += i;
            }
        }
        System.out.println("\nSum of odd numbers: " + sumOdd);
        System.out.println("Sum of even numbers: " + sumEven);
        System.out.println("Print size of set:");
        int n = sc.nextInt();
        if (n < 1) {
            System.out.println("Error");
        } else {
            long biggestOdd = -1;
            long biggestEven = -1;
            int countOdd = 0;
            int countEven = 0;
            for (int i = n - 1; 0 <= i; i--) {
                int number = Fibonacci.getFibonacci(i);
                System.out.println(number);
                if (number % 2 != 0 && biggestEven == -1) {
                    biggestEven = number;
                    countEven += 1;
                } else if (number % 2 == 0 && biggestOdd == -1) {
                    biggestOdd = number;
                    countOdd += 1;
                } else if (number % 2 == 0) {
                    countOdd += 1;
                } else {
                    countEven += 1;
                }
            }
            double oddPercentage = ((double) countOdd / n) * percentage;
            double evenPercentage = ((double) countEven / n) * percentage;
            System.out.println("F1: " + biggestOdd);
            System.out.println("F2: " + biggestEven);
            System.out.println("Percentage of odd numbers: " + oddPercentage);
            System.out.println("Percentage of even numbers: " + evenPercentage);
        }
    }
}
